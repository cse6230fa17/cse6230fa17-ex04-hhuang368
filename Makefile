EXENAME = ex04

CC      = g++
CFLAGS  = -O3 -Wall -fopenmp
LDFLAGS = 
RM      = rm -f

CPPSRCS = $(wildcard *.cc)
OBJS    = $(CPPSRCS:.cc=.o)

build : $(EXENAME)

$(EXENAME): $(OBJS) 
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(EXENAME) $(OBJS)

%.o: %.cc
	$(CC) $(CFLAGS) -c $^

clean:
	$(RM) *.o $(EXENAME)