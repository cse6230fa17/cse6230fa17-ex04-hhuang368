#include "forces.h"
#include <omp.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

double *local_forces    = NULL;
int local_forces_copies = 0;
int local_forces_num    = 0;

void init_local_mem(const int N)
{
	int nthreads = omp_get_max_threads();
	if ((nthreads > local_forces_copies) || (N > local_forces_num))
	{
		local_forces_copies = nthreads;
		local_forces_num = N;
		if (local_forces != NULL) free(local_forces);
		local_forces = (double*) malloc(sizeof(double) * 3 * N * nthreads);
	}
	
	#pragma omp parallel for schedule(static)
	for (int i = 0; i < 3 * N * local_forces_copies; i++)
		local_forces[i] = 0.0;
}

void compute_forces_critical(int N, const double *pos, double *forces)
{
	#pragma omp parallel for schedule(SCHEDULER)
	for (int i = 0; i < 3 * N; i++) forces[i] = 0.0;

	#pragma omp parallel for schedule(SCHEDULER)
	for (int i = 0; i < N; i++)
	{
		const double *ri = &pos[3 * i];
		double *forces_i = forces + 3 * i;
		
		for (int j = 0; j < N; j++)
		{
			if (j == i) continue;
			
			const double *rj = &pos[3 * j];
				
			double dx = remainder(ri[0] - rj[0], 1.0);
			double dy = remainder(ri[1] - rj[1], 1.0);
			double dz = remainder(ri[2] - rj[2], 1.0);
			
			double s2 = dx * dx + dy * dy + dz * dz;
			if (s2 < 4.0 * Radius * Radius)
			{
				double s = sqrt(s2);
				double F = Kr * (2.0 * Radius - s) / s;
				double *forces_j = forces + 3 * j;
				
				dx *= F;
				dy *= F;
				dz *= F;
				
				#pragma omp critical
				{
					forces_i[0] += dx;
					forces_i[1] += dy;
					forces_i[2] += dz;
					
					forces_j[0] -= dx;
					forces_j[1] -= dy;
					forces_j[2] -= dz;	
				}
			}
		}
	}
}

void compute_forces_atomic(int N, const double *pos, double *forces)
{
	#pragma omp parallel for schedule(SCHEDULER)
	for (int i = 0; i < 3 * N; i++) forces[i] = 0.0;

	#pragma omp parallel for schedule(SCHEDULER)
	for (int i = 0; i < N; i++)
	{
		const double *ri = &pos[3 * i];
		double *forces_i = forces + 3 * i;
		
		for (int j = 0; j < N; j++)
		{
			if (j == i) continue;
			
			const double *rj = &pos[3 * j];
				
			double dx = remainder(ri[0] - rj[0], 1.0);
			double dy = remainder(ri[1] - rj[1], 1.0);
			double dz = remainder(ri[2] - rj[2], 1.0);
			
			double s2 = dx * dx + dy * dy + dz * dz;
			if (s2 < 4.0 * Radius * Radius)
			{
				double s = sqrt(s2);
				double F = Kr * (2.0 * Radius - s) / s;
				double *forces_j = forces + 3 * j;
				
				dx *= F;
				dy *= F;
				dz *= F;
				
				#pragma omp atomic
				forces_i[0] += dx;
				#pragma omp atomic
				forces_i[1] += dy;
				#pragma omp atomic
				forces_i[2] += dz;
				
				#pragma omp atomic
				forces_j[0] -= dx;
				#pragma omp atomic
				forces_j[1] -= dy;
				#pragma omp atomic
				forces_j[2] -= dz;
			}
		}
	}
}

void compute_forces_1side(int N, const double *pos, double *forces)
{
	#pragma omp parallel for schedule(SCHEDULER)
	for (int i = 0; i < N; i++)
	{
		const double *ri = &pos[3 * i];
		double *forces_i = forces + 3 * i;
		forces_i[0] = forces_i[1] = forces_i[2] = 0.0;
		
		for (int j = 0; j < N; j++)
		{
			if (j == i) continue;
			
			const double *rj = &pos[3 * j];
				
			double dx = remainder(ri[0] - rj[0], 1.0);
			double dy = remainder(ri[1] - rj[1], 1.0);
			double dz = remainder(ri[2] - rj[2], 1.0);
			
			double s2 = dx * dx + dy * dy + dz * dz;
			if (s2 < 4.0 * Radius * Radius)
			{
				double s = sqrt(s2);
				double F = Kr * (2.0 * Radius - s) / s;
				
				forces_i[0] += F * dx;
				forces_i[1] += F * dy;
				forces_i[2] += F * dz;
			}
		}
	}
}

void compute_forces_dupbuf(int N, const double *pos, double *forces)
{
	init_local_mem(N);
	
	#pragma omp parallel
	{
		int tid = omp_get_thread_num();
		double *forces_copy = local_forces + 3 * N * tid;
	
		#pragma omp for schedule(SCHEDULER)
		for (int i = 0; i < N; i++)
		{
			const double *ri = &pos[3 * i];
			double *forces_i = forces_copy + 3 * i;
			for (int j = i + 1; j < N; j++)
			{
				const double *rj = &pos[3 * j];
				
				double dx = remainder(ri[0] - rj[0], 1.0);
				double dy = remainder(ri[1] - rj[1], 1.0);
				double dz = remainder(ri[2] - rj[2], 1.0);
				
				double s2 = dx * dx + dy * dy + dz * dz;
				if (s2 < 4.0 * Radius * Radius)
				{
					double s = sqrt(s2);
					double F = Kr * (2.0 * Radius - s) / s;
					double *forces_j = forces_copy + 3 * j;
					
					dx *= F;
					dy *= F;
					dz *= F;
					
					forces_i[0] += dx;
					forces_i[1] += dy;
					forces_i[2] += dz;
								
					forces_j[0] -= dx;
					forces_j[1] -= dy;
					forces_j[2] -= dz;	
				}
			}
		}
	}
	
	memset(forces, 0, sizeof(double) * 3 * N);
	for (int i = 0; i < local_forces_copies; i++)
	{
		double *forces_copy = local_forces + 3 * N * i;
		#pragma omp parallel for
		for (int j = 0; j < 3 * N; j++)
			forces[j] += forces_copy[j];
	}
}

void compute_forces(int N, const double *pos, double *forces)
{
	// compute_forces_critical(N, pos, forces);
	// compute_forces_atomic(N, pos, forces);
	compute_forces_1side(N, pos, forces);
	// compute_forces_dupbuf(N, pos, forces);
}

void release_forces_mem()
{
	if (local_forces != NULL) free(local_forces);
}
