#!/bin/bash -l

#SBATCH -N 1
#SBATCH -t 0:20:00
#SBATCH -e build.log
#SBATCH -o build.log
#SBATCH --open-mode truncate

make clean
make

date

if [[ -x "ex04" ]]
then
  export OMP_NUM_THREADS=4
  ./ex04
  date
fi

