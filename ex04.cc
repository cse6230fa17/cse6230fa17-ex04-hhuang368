#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <assert.h>
#include <omp.h>

#include "forces.h"

const double inv_RAND_MAX = 1.0 / (double) RAND_MAX;
const double time_step    = 1e-4;
const double bm_step      = sqrt(2.0 * time_step);

int particle_num    = 10000;
int total_steps		= 5000;
int snapshot_period = 500;

/*
inline double rand_double(const double min, const double max)
{
	double range = max - min;
	double scale = (double)rand() * inv_RAND_MAX;
	return (min + scale * range);
}
*/

inline double rand_01()
{
	return (double)rand() * inv_RAND_MAX;
}

inline double rand_pm1()
{
	return (2.0 * (double)rand() * inv_RAND_MAX - 1.0);
}

inline double remainder_01(const double x)
{
	if (x > 1.0) return (x - 1.0);
	if (x < 0.0) return (x + 1.0);
	return x;
}

inline double get_ith_distance(double *coords, double *coords_0, const int i)
{
	int coord_base = i * 3;
	double dx = coords[coord_base]     - coords_0[coord_base];
	double dy = coords[coord_base + 1] - coords_0[coord_base + 1];
	double dz = coords[coord_base + 2] - coords_0[coord_base + 2];
	return sqrt(dx * dx + dy * dy + dz * dz);
}

int main(int argc, char **argv)
{
	if (argc >= 2) total_steps     = atoi(argv[1]);
	if (argc >= 3) snapshot_period = atoi(argv[2]);
	if (argc >= 4) particle_num    = atoi(argv[3]);
	printf("Simulate %d steps and snapshot %d particles' distance every %d steps.\n", total_steps, particle_num, snapshot_period);
	
	srand(time(NULL));
	
	double *coords   = (double*) malloc(sizeof(double) * 3 * particle_num);
	double *coords_0 = (double*) malloc(sizeof(double) * 3 * particle_num);
	double *forces   = (double*) malloc(sizeof(double) * 3 * particle_num);
	assert((coords != NULL) && (coords_0 != NULL) && (forces != NULL));

	int nthreads = 1;
	#pragma omp parallel
	{
		#pragma omp master
		nthreads = omp_get_num_threads();
	}
	double *distances = (double*) malloc(sizeof(double) * nthreads);
	printf("We are using %d threads for computing. \n", nthreads);
	
	#pragma omp parallel for schedule(SCHEDULER)
	for (int i = 0; i < particle_num * 3; i++)
	{
		coords_0[i] = rand_01();
		coords[i] = coords_0[i];
		forces[i] = 0.0;
	}

	
	double st = omp_get_wtime();
	
	int iter = 0;
	while (iter < total_steps)
	{
		#pragma omp parallel for 
		for (int i = 0; i < particle_num * 3; i++)
		{
			coords[i] += bm_step * rand_pm1();
			coords[i]  = remainder_01(coords[i]);
		}
		
		compute_forces(particle_num, coords, forces);
		
		#pragma omp parallel for
		for (int i = 0; i < particle_num * 3; i++)
		{
			coords[i] += time_step * forces[i];
			coords[i]  = remainder_01(coords[i]);
		}
		
		iter++;
		if (iter % snapshot_period == 0)
		{
			#pragma omp parallel 
			{
				int tid = omp_get_thread_num();
				distances[tid] = 0;
			
				#pragma omp for
				for (int i = 0; i < particle_num; i++)
					distances[tid] += get_ith_distance(coords, coords_0, i);
			}
			
			for (int i = 1; i < nthreads; i++) 
				distances[0] += distances[i];
			
			distances[0] /= (double)(particle_num);
			
			double et = omp_get_wtime();
			
			printf("Average distance after %4d timesteps: %lf, elapsed time = %lf (s)\n", iter, distances[0], et - st);
		}
	}
	
	double et = omp_get_wtime();
	printf("Elapsed time = %lf seconds.\n", et - st);
	
	release_forces_mem();
	
	free(distances);
	free(forces);
	free(coords_0);
	free(coords);
	
	return 0;
}
