
#ifndef SCHEDULER
#define SCHEDULER dynamic,4
#endif

const double Kr     = 100.0;
const double Radius = 0.2;

/* compute repulsive forces for N particles in 3D */
void compute_forces(int N, const double *pos, double *forces);

void release_forces_mem();
